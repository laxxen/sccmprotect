﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SCCMProtect
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            String Password = "";

            if (args.Length == 0)
                Password = "";
            else
                Password = args[0];

            Application.Run(new frmMain(Password));
        }
    }
}
